import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './components/login.component';
import Dashboard from './components/dashboard.component';
import Barang from './components/barang.component'
import SimplePaper from './components/paper/simplePaper';
import PaginationTable from './components/table/PaginationTable';


class App extends Component {

  render() {
      return (
        <div className="App">
          <Router>
            <Switch>
              <Route exact path='/' component={Login} />
              <Route path='/index' component={Login} />
              <Route path='/dashboard' component={Dashboard}/>
              <Route path='/barang' component={Barang}/>
              <Route path='/paper' component={SimplePaper} />
              <Route path='/paginationTable' component={PaginationTable} />
            </Switch>
          </Router>
        </div>
      );
  }
}

export default App;

