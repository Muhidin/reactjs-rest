import React from 'react';
import axios from 'axios';

class Login extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isSubmited: false,
            username: '',
            password: '',
            authError: false,
        }
    }

    usernameChange(event) {
        const inputVal = event.target.value;

        this.setState({
            username: inputVal,
        })
    }

    passChange(event) {
        const inputVal = event.target.value;

        this.setState({
            password: inputVal,
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        const url = 'http://localhost/resto-pos-api/index.php/auth/login';
        const username = this.state.username;
        const password = this.state.password;

        let bodyFormData = new FormData();
        bodyFormData.set('username', username);
        bodyFormData.set('password', password);

        axios.post(url, bodyFormData)
            .then(result => {

            })
    }

}
export default Login;