import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(1),
      width: theme.spacing(20),
      height: theme.spacing(20),
    },
  },
}));

function Header(header) {
    return <div> <h1> {this.props.header } </h1></div>
}

export default function SimplePaper() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {/* <Paper elevation={0} /> */}
      <Paper />
      <Paper elevation={3} />
      <Paper elevation={3} />
      <Paper elevation={3} />
      <Paper elevation={3} />
      <Paper elevation={3} />
      <Paper elevation={3} />
      <Paper elevation={3} contentEditable={Header("TEST")} />

    </div>
  );
}
