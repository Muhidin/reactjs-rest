import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

import TextField from '@material-ui/core/TextField';
import { red } from '@material-ui/core/colors';
import Button from '@material-ui/core/Button';
import qs from 'qs';


export default class Login extends Component {

    
    state = {
        username: '',
        password: '',
        redirect: false,
        authError: false,
        isLoading: false
    };


    handleUsernameChange = event => {
        this.setState({username: event.target.value});
    };

    handlePwdChange = event => {
        this.setState({password: event.target.value});
    };

    handleSubmit = event => {
        event.preventDefault();
        this.setState({isLoading: true});
        // const url = 'http://localhost/resto-pos-api/index.php/auth/login';
        const url = 'http://localhost:4000/auth/login';
        // const username = this.state.username;
        // const password = this.state.password;

        // let bodyFormData = new FormData();
        // bodyFormData.set('username', username);
        // bodyFormData.set('password', password);

        // axios.post(url, bodyFormData)
        //     .then(result => {
        //         console.log(result.data.status);
        //         if (result.data.status === true) {
        //             console.log(result.data);
        //             localStorage.setItem('token', result.data.token);
        //             this.setState({redirect: true, isLoading: false});
        //             localStorage.setItem('isLoggedIn', true);

        //             console.log('success login');

        //         } else {
        //             this.setState({isLoading: false});
        //             console.log(result.data);
        //             alert(result.data.message);
        //         }
        //     })
        //     .catch( error => {
        //         console.log(error);
        //         this.setState({authError: true, isLoading: false});
                
        //     });

            //             var axios = require('axios');
            // var qs = require('qs');
            // var data = qs.stringify({
            // 'username': 'mumu',
            // 'password': 'J13071994!!' 
            // });

            let data1 = qs.stringify({
                'username' : this.state.username,
                'password' : this.state.password
            })
            //data1.push({username: this.state.username, password: this.state.password})
            var config = {
            method: 'post',
            url: 'http://localhost:4000/auth/login',
            headers: { 
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data : data1
            };

            axios(config)
            .then(function (response) {
            console.log(JSON.stringify(response.data));
            })
            .catch(function (error) {
            console.log(error);
            });
    };

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/dashboard'/>
        }
    };

    

    render() {
        const isLoading = this.state.isLoading;
        return (
            <div className="container">
                {/* title here */}

                <div className="card card-login mx-auto md-4">
                    <div className="card-header">Login</div>
                    <div className="card-body">
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <div className="form-label-group">
                                    {/* <input className={"form-control " + (this.state.authError ? 'is-invalid' : '')} id="inputUsername" placeholder="Username" type="text" name="username" onChange={this.handleUsernameChange} autoFocus required/> */}
                                    <TextField style={{ color: red }} id="inputUsername" label="username" type="text" name="username" onChange={this.handleUsernameChange} className={"form-control " + (this.state.authError ? 'is-invalid' : '')} autoFocus required />
                                    <div className="invalid-feedback">
                                        Please provide a valid username.
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="form-label-group">
                                    <TextField type="password" 
                                    className={"form-control " + (this.state.authError ? 'is-invalid' : '')} 
                                    id="inputPassword" label="password" 
                                    name="password" onChange={this.handlePwdChange} required/>
                                    <div className="invalid-feedback">
                                        Please provide a valid Password.
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="checkbox">
                                    <label>
                                        <input type="checkbox" value="remember-me"/>Remember Password
                                    </label>
                                </div>
                            </div>
                            <div className="form-group">
                                {/* <button className="btn btn-primary btn-block" type="submit" disabled={this.state.isLoading ? true : false}>Login &nbsp;&nbsp;&nbsp;
                                    {isLoading ? (
                                        <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    ) : (
                                        <span></span>
                                    )}
                                </button> */}
                                <Button variant="contained" color="primary" type="submit" disabled={this.state.isLoading ? true : false }>Login &nbsp;&nbsp;&nbsp;
                                        {
                                            isLoading ? ( <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>) : (<span></span>)
                                        }
                                </Button>
                            </div>
                            
                        </form>

                    </div>
                </div>
                {this.renderRedirect()}

            </div>
        )
    }
}