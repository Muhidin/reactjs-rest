import React, { Component } from 'react';
import {  Redirect } from 'react-router-dom';
import  axios from 'axios';

export default class Barang extends Component {
    state = {
        databarang: [],
        toDashboard: false,
        isLoading: false,
        perpage: 10,
        page:0,
    };

    constructor(props)  {
        super(props)
        this.url = 'http://localhost/ci-3-API/index.php/api/barang?pagenum=0&pagesize=10';
        //this.token = localStorage.getItem('token');
    }

    componentDidMount() {
        axios.get(this.url)
            .then(response => {
                //console.log(response);
                const databarang = response.data.data;
                //console.log(databarang)
                this.setState({ databarang });
            })
            .catch(error => {
                this.setState({ toDashboard: true });
                //console.log(error);
            });
    }

    // handle delete here


    render() {
        // if (this.state.toDashboard === true) {
        //     return <Redirect to='/' />
        // }
        return(
            <div>
                <div className="card mb-3">
                    <div className="card-header"><i className="fas fa-table"></i> 
                        &nbsp;&nbsp;Data Barang
                    </div>

                    <div className="card-body">
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Nama Barang</th>
                                    <th>Harga</th>
                                    <th>Stok</th>
                                    <th>Unit</th>
                                    <th>Kategori</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                {this.state.databarang.map( (barang) =>
                                    <tr key={barang.kode_brg}>
                                        <td>{barang.kode_brg}</td>
                                        <td>{barang.nama_brg}</td>
                                        <td>{barang.harga}</td>
                                        <td>{barang.stok}</td>
                                        <td>{barang.unit}</td>
                                        <td>{barang.kategori}</td>
                                        <td className="text-center">
                                            <button><span className="glyphicon glyphicon-trash"></span> Delete</button>
                                        </td>
                                    </tr>)
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}