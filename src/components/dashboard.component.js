import React, { Component } from 'react';
import {Link , Redirect} from 'react-router-dom';
import Header from './header.component';

import { decodeJWT } from './decodeToken.component';


export default class Dashboard extends Component {
    render() {

        let token = localStorage.getItem('token')

        if(!token) {
            return (
                <Redirect to='/'/>
            )
        } else {

            const  data_user = decodeJWT(token);

            //console.log(data_user);
            localStorage.setItem('username', data_user.username);
            localStorage.setItem('id_user', data_user.id_user);
            localStorage.setItem('expired_in', data_user.expired_in);
            

            return (
                <div>
                    <Header />
                    <Link to={'/barang'}>Barang</Link>
                    <Link to={'/paper'}>Paper</Link>
                    <Link to={'/paginationTable'}>Paper</Link>
                </div>
            )

        }

    }
}